# Book Annotations

Probably more appropriately named Book Notes, this app allows you to search for books using Google's Books API and creates notebooks with notebook pages corresponding to each page in your books. This allows the user to summarize and write notes page by page to retain more information as they read. 


# WIP
This repository is a Work In Progress